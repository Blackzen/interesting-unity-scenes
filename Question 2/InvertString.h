#ifndef FIRSTDLL_NATIVE_LIB_H
#define FIRSTDLL_NATIVE_LIB_H

#define DLLExport __declspec(dllexport)
#include<iostream>

using namespace std;
extern "C"
{
    DLLExport int add(int num1, int num2);
    DLLExport int multiply(int num1, int num2);
    DLLExport int substract(int num1, int num2);
    DLLExport int divide(int num1, int num2);
    DLLExport int TestInvert(char* str);
    DLLExport int CountLetters(char* str);
}
#endif